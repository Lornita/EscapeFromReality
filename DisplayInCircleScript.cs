using UnityEngine;
using System.Collections;
using UnityEditor;

public class DisplayInCircleScript : MonoBehaviour
{
    public float mouseSensitivity  = 1.0f;
    private Vector3 lastPosition;
    public float speed = 10000f;
    // Use this for initialization
    void Start()
    {
    GameObject[] gos= GameObject.FindGameObjectsWithTag("Cube");
         
            int numberOfObjects = gos.Length;
            //go.AddComponent<NewBehaviourScript>();
            float startAngle = (Mathf.PI / numberOfObjects);
            float theta = startAngle*2;
            float buffer = 11f - 0.5f * numberOfObjects;
            int radius = 8;
            radius += numberOfObjects;

        
        foreach (GameObject go in gos)
        {
            float posx = radius * Mathf.Cos(theta);
            float posy = 1.2f;
            float posz = buffer + radius * Mathf.Sin(theta);
            Vector3 pos = new Vector3(posx, posy, posz);

            go.transform.position = (pos);
            theta += startAngle * 2;

        }

    }

    // Update is called once per frame
    void Update()
    {
      
        transform.Rotate(Vector3.up, speed * Time.deltaTime);
     
        }

}
