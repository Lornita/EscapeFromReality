package escapefromreality.poc0.restapi;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner demo(PersonRepository repository) {
		return (args) -> {

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

			// save a couple of customers
			repository.save(new Person("firstName0", "lastName0", "text00", "text10", "text20", 
					new Double(0), new Double(1000), new Double(2000), sdf.parse("01-01-1960"), sdf.parse("01-01-1970"),
					sdf.parse("01-01-1980")));
			repository.save(
					new Person("firstName1", "lastName1", "text01", "text11", "text21", new Double(2000),
							new Double(3000), new Double(4000), sdf.parse("01-01-1961"), sdf.parse("01-01-1971"),
							sdf.parse("01-01-1981")));
			repository.save(
					new Person("firstName2", "lastName2", "text00", "text12", "text22", new Double(4000),
							new Double(5000), new Double(6000), sdf.parse("01-01-1962"), sdf.parse("01-01-1972"),
							sdf.parse("01-01-1982")));
			repository.save(
					new Person("firstName3", "lastName3", "text03", "text13", "text23", new Double(6000),
							new Double(7000), new Double(8000), sdf.parse("01-01-1963"), sdf.parse("01-01-1973"),
							sdf.parse("01-01-1983")));

			// fetch all customers
			log.info("People found with findAll():");
			log.info("-------------------------------");
			for (Person person : repository.findAll()) {
				log.info(person.toString());
			}
            log.info("");

		};
	}
}
