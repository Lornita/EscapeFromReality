package escapefromreality.poc0.restapi;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String firstName;
	private String lastName;

	private String text0;
	private String text1;
	private String text2;

	private Double number0;
	private Double number1;
	private Double number2;

	private Date date0;
	private Date date1;
	private Date date2;


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getText0() {
		return text0;
	}

	public void setText0(String text0) {
		this.text0 = text0;
	}

	public String getText1() {
		return text1;
	}

	public void setText1(String text1) {
		this.text1 = text1;
	}

	public String getText2() {
		return text2;
	}

	public void setText2(String text2) {
		this.text2 = text2;
	}

	public Double getNumber0() {
		return number0;
	}

	public void setNumber0(Double number0) {
		this.number0 = number0;
	}

	public Double getNumber1() {
		return number1;
	}

	public void setNumber1(Double number1) {
		this.number1 = number1;
	}

	public Double getNumber2() {
		return number2;
	}

	public void setNumber2(Double number2) {
		this.number2 = number2;
	}

	public Date getDate0() {
		return date0;
	}

	public void setDate0(Date date0) {
		this.date0 = date0;
	}

	public Date getDate1() {
		return date1;
	}

	public void setDate1(Date date1) {
		this.date1 = date1;
	}

	public Date getDate2() {
		return date2;
	}

	public void setDate2(Date date2) {
		this.date2 = date2;
	}

	public Person() {
		super();
	}

	public Person(String firstName, String lastName, String text0, String text1, String text2, Double number0,
			Double number1, Double number2, Date date0, Date date1, Date date2) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.text0 = text0;
		this.text1 = text1;
		this.text2 = text2;
		this.number0 = number0;
		this.number1 = number1;
		this.number2 = number2;
		this.date0 = date0;
		this.date1 = date1;
		this.date2 = date2;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", text0=" + text0
				+ ", text1=" + text1 + ", text2=" + text2 + ", number0=" + number0 + ", number1=" + number1
				+ ", number2=" + number2 + ", date0=" + date0 + ", date1=" + date1 + ", date2=" + date2 + "]";
	}

}
