/*
This script can be used to allow the user enter a number.  
The user can look at the box and tap the side of the Gear VR and the number displayed will go up or down depending on which box they are looking at.

Steps
Create an empty GameObject.
Attached two cubes to GameObject.
Call them UpAge and DownAge.
Add a 3D Text Object between the UpAge and DownAge boxes.
Attach this script to the empty GameObject
*/

﻿using UnityEngine;
using System.Collections;

public class AgeArrowClick : MonoBehaviour {
	double age=21;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		TextMesh textObject = GameObject.Find ("AgeText").GetComponent<TextMesh> ();
		if (Input.GetMouseButtonDown (0)) { // if left button pressed...
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {
				Debug.Log ("You clicked on " + hit.transform.gameObject.name);
				if (hit.transform.gameObject.name == "UpAge") {
					//increase the value if AgeText					 
					age++;
					textObject.text = age.ToString();
				}
				if (hit.transform.gameObject.name == "DownAge") {
					//increase the value if AgeText					 
					age--;
					textObject.text = age.ToString();
				}
			}
		}

	}
}
