using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AdjustHeight : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        GameObject[] gos = GameObject.FindGameObjectsWithTag("Cylinder");
        GameObject car = GameObject.FindGameObjectWithTag("Player");
        GameObject fps = GameObject.FindGameObjectWithTag("Cube");
        Text text = GetComponent<Text>();
        setScale(gos);
        lineUpObjects(gos, car, fps);

        text.text = "Balance: $185,000, Age: 40";
    }

    private static void setScale(GameObject[] gos)
    {
        float posy = 1;
        foreach (var go in gos)
        {
            posy *= 1.5f;
            float posx = 1;
            float posz = 1;
            Vector3 pos = new Vector3(posx, posy, posz);

            go.transform.localScale = (pos);
        }
    }

    private static void lineUpObjects(GameObject[] gos, GameObject car, GameObject fps)
    {
        Quaternion rotation = Quaternion.Euler(0, 90, 0);
        car.transform.rotation = rotation;

        int numberOfObjects = gos.Length;
        float posx = 0;
        foreach (GameObject go in gos)
        {
            posx += 5;
            float posy = 1.2f;
            float posz = 0;
            Vector3 pos = new Vector3(posx, posy, posz);

            go.transform.position = (pos);


        }

        float z = car.transform.position.z * -1;
        float y = car.transform.position.y;
        car.transform.position = gos[0].transform.position;
        float x = car.transform.position.x;
        Vector3 p = new Vector3(x, y, z);
        car.transform.position = p;


        fps.transform.position = new Vector3(x, y, (z + 12));
    }

    // Update is called once per frame
    void Update () {
	
	}
}
